#!/usr/bin/env python
from .generate_mesh import rectangle, fsaverage_dir
import scipy as sp
import tempfile
import subprocess
import os
from .. import mesh
from nibabel import gifti

def test_normals():
    size = [2., 2.52, 1.3321]
    mesh = rectangle(size)
    norm = mesh.normal()
    assert norm.shape == (3, mesh.nfaces)
    assert sp.sum(norm) == 0
    assert (sp.sum(norm ** 2, 0) == 1).all()
    for ixdim in range(3):
        for ixface in range(2):
            use_vertex = sp.all(mesh.vertices[ixdim, :][mesh.faces] == ixface * size[ixdim], 0)
            assert sp.sum(use_vertex) == 2
            if ixface == 0:
                assert (norm[ixdim, use_vertex] == 1).all()
            else:
                assert (norm[ixdim, use_vertex] == -1).all()
    assert mesh.normal(atpoint=True).shape == (3, mesh.nvertices)
    assert sp.amax(abs(sp.sum(mesh.normal(atpoint=True) ** 2, 0) - 1)) < 1e-5


def test_point_conn_graph():
    mesh = rectangle(sp.ones(3))
    conn_pos = (mesh.graph_connection_point().T * mesh.vertices.T).T
    assert (sp.unique(conn_pos) == sp.arange(4)).all()
    assert (sp.sum((conn_pos == 0) | (conn_pos == 3), 0) == 1).all()

    assert mesh.graph_connection_point('i4').dtype == 'i4'
    assert mesh.graph_connection_point('f4').dtype == 'f4'
    assert mesh.graph_connection_point('bool').dtype == 'bool'

    assert (mesh.graph_connection_point().sum(0) == 3).all()


def test_affine():
    mesh = rectangle(sp.ones(3))
    new_mesh = mesh.apply_affine(sp.eye(4) * 2)
    assert (new_mesh.vertices == rectangle(sp.ones(3) / 2).vertices).all()
    new_mesh = mesh.apply_affine(sp.eye(4) / 3)
    assert (new_mesh.vertices == rectangle(sp.ones(3) * 3).vertices).all()


def test_mesh_size():
    file, metric_filename = tempfile.mkstemp('.shape.gii')
    surf_filename = os.path.join(fsaverage_dir, '100000.L.pial.32k_fs_LR.surf.gii')
    subprocess.call('wb_command -surface-vertex-areas %s %s' % (surf_filename, metric_filename), shell=True)
    wb_data = gifti.read(metric_filename).darrays[0].data
    py_data = mesh.Mesh2D.read(surf_filename).size_vertices()
    assert sp.amax(abs(py_data - wb_data)) < 1e-3


