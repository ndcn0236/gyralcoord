from .. import cortex
import os


def read_cortex():
    dirname = os.path.join(os.path.split(__file__)[0], 'fsaverage')
    return cortex.read_HCP(dirname)


def test_base():
    right, left = read_cortex()
    assert right.hemisphere == 'right'
    assert left.hemisphere == 'left'
    for cort in right, left:
        assert cort.primary == 'cortex'
        assert len(cort.surfaces) == 3
        for surf, name in zip(cort.surfaces, ('graywhite', 'midthickness', 'pial')):
            assert surf.anatomy.primary == "cortex"
            assert surf.anatomy.secondary == name


def test_volume():
    right, left = read_cortex()
    print(right.volume())
