import scipy as sp
from ..mesh import Mesh2D
from ..cortex import Cortex
from ..cortical_mesh import CorticalMesh
from operator import xor
import os
from scipy import spatial

fsaverage_dir = os.path.join(os.path.split(__file__)[0], 'fsaverage')


def sphere(norient=300):
    """Generates a mesh of a unit sphere with `norient` vertices per hemisphere.
    """
    import dipy
    rand_orient = sp.randn(3, norient)
    rand_orient /= sp.sqrt(sp.sum(rand_orient ** 2, 0))
    hsph_initial = dipy.core.sphere.HemiSphere(*rand_orient)
    hsph_dispersed, _ = dipy.core.sphere.disperse_charges(hsph_initial, 3000)

    points = sp.concatenate([hsph_dispersed.vertices, -hsph_dispersed.vertices], 0)
    ch = spatial.ConvexHull(points)
    mesh = Mesh2D(ch.points.T, ch.simplices.T)

    meanp = mesh.vertices[:, mesh.faces].mean(1)
    flip = (meanp * mesh.normal()).sum(0) < 0
    mesh.faces[:2, flip] = mesh.faces[1::-1, flip]
    return Mesh2D(mesh.vertices, mesh.faces)


def create_spherical_cortex(norient=300):
    white = sphere(norient)
    pial = Mesh2D(white.vertices * 2, white.faces)
    return Cortex([white, pial])


def rectangle(size=(1., 1., 1.), inward_normals=True):
    """generate a rectangle as triangular mesh

    :param size: rectangle size (in arbitrary units)
    :param inward_normals: ensure the normals are pointing inwards (otherwise they point in random directions.
    :return: triangular mesh covering the surface of a rectangle
    :rtype: Mesh2D
    """
    points = sp.array(sp.broadcast_arrays(sp.arange(2)[:, None, None],
                                          sp.arange(2)[None, :, None],
                                          sp.arange(2)[None, None, :])).reshape((3, 8))
    vertices = sp.zeros((3, 12), dtype='i4')
    for ixdim in range(3):
        ixdim_other = list(range(3))
        ixdim_other.remove(ixdim)
        for ixface in range(2):
            use_points = points[ixdim, :] == ixface
            assert sp.sum(use_points) == 4
            ixpoint1 = sp.where(use_points)[0][:1]
            ixneigh = sp.where(use_points & (sp.sum(abs(points - points[:, ixpoint1]), 0) == 1))[0]
            ixpoint2 = sp.where(use_points & (sp.sum(abs(points - points[:, ixpoint1]), 0) == 2))[0]
            if inward_normals:
                offset = points[:, ixneigh] - points[:, ixpoint1]
            assert (sp.cross(offset[:, 0], offset[:, 1])[ixdim_other] == 0).all()
            assert (abs(sp.cross(offset[:, 0], offset[:, 1])[ixdim]) == 1).all()
            if xor(sp.cross(offset[:, 0], offset[:, 1])[ixdim] < 0, ixface == 1):
                ixneigh = ixneigh[::-1]
            vertices[:, ixdim + ixface * 3] = sp.append(ixpoint1, ixneigh)
            vertices[:, ixdim + ixface * 3 + 6] = sp.append(ixpoint2, ixneigh[::-1])
    mesh = Mesh2D(points * sp.array(size)[:, None], vertices)
    assert mesh.nvertices == 8
    assert mesh.nfaces == 12
    assert mesh.ndim == 3
    return mesh


def gifti():
    filename = os.path.join(fsaverage_dir, '100000.L.pial.32k_fs_LR.surf.gii')
    return CorticalMesh.read(filename)
