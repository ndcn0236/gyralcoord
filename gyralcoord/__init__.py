"""Utilities for working with cortical meshes and defining the gyral coordinate system

Contains:

- `cortex`: classes to work with multiple cortical mesh spanning a cortical layer
- `grid`: many functions that require a grid and a cortical mesh
- `mesh`: basis class (`Mesh2D`) to deal with a mesh
- `cortical_mesh`: extends `Mesh2D` into `CorticalMesh`, which contains metadata with the anatomy described by the mesh
- `orientation`: computes the gyral coordinate system
- `radial_transition`: quantifies the transition boundary from tangential orientations in the white matter to radial orientations in the cortex
- `utils` few utility functions
"""
from .cortical_mesh import CorticalMesh, BrainStructure
from .mesh import Mesh2D
from .cortex import Cortex, CorticalLayer, read_HCP
from . import grid
