# Defining gyral coordinates
At every voxel in the cortex and superficial white matter the gyral coordinate system defines three orthonormal axes:
* The *radial* axis tends to be orthogonal to the white/grey matter boundary and pial surface and is defined by interpolating the surface normals.
* The *sulcal* axis tends to point between the deep white matter to the gyral crown and is defined by interpolating the sulcal depth gradient
* The *gyral* axis tends to align with the long axis of the gyrus and is defined as the cross product of the other two

Although the gyral coordinate system is defined purely based on the morphology of the brain, it is highly predictive of orientations estimated from diffusion MRI data (Cottaar et al., in prep)

The code can be installed in the default way (i.e., downloading the source from github and running `pip install .` or `python setup.py install` in the main directory).

## Scripts
When installed this program will provide 4 scripts to compute and work with the gyral coordinates from the command line:
* `gcoord_gen`: Given a white/grey matter boundary and pial surface (in GIFTI format) computes the gyral coordinate system at every voxel in a reference image using linear (recommended), non-linear, or nearest-neighbour interpolation.
* `gcoord_mult`: Converts dyads expressed in the stereotaxic coordinate system to the gyral coordinate system
* `gcoord_split`: The gyral coordinate system from `gcoord_gen` is stored as a 3x3 matrix for every voxel. For visualization this script splits this 3x3 matrix into 3 images containing the radial, sulcal, and gyral it can be helpful to store the radial, sulcal, and gyral axes.
* `gcoord_transition`: Measures the location and width of the transition from tangential orientations in the superficial white matter to radial orientations in the cortex

## Library access
For more advanced usage, one can access the underlying library directly in python. This library contains the following files:
* `gyralcoord/orientation`: Contains the main code to compute the gyral coordinate system (used by `gcoord_gen` script)
* `gyralcoord/radial_transition`: Contains the main code to compute the tangential to radial transition boundary (used by `gcoord_transition` script)

Several other classes and methods to work with surface data are provided as well:
- `cortical_mesh.py`: any functions based on single (cortical) mesh, such as:
  - input/output from GIFTI
  - distance along the surface
  - normals
  - projecting surface onto a plane for plotting
- `cortex.py`: any functions based on multiple meshed spanning a full cortex
  - input from fsaverage directory from HCP
  - volume calculations (either per face/vertex or total)
  - finding the closest cortical surface element
  - predicting the gyral bias
  - segmenting a volume based on the cortical layers
- `grid.py`: any functions projecting from a surface onto a volume or vice versa
  - defining a volume with given resolution covering the full mesh
  - grid intersection
  - distance calculation from the grid (requires wb_command)
  - ray intersection with a mesh (uses the grid intersection to greatly speed up the calculation)
