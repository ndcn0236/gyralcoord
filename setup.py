#!/usr/bin/env python

from setuptools import setup

with open('requirements.txt', 'r') as f:
    dependencies = [line.strip() for line in f.readlines()]

setup(name='gyralcoord',
      version='0.2',
      description='computes a gyral coordinate system (as well as many utilities to work with meshes)',
      author='Michiel Cottaar',
      author_email='Michiel.Cottaar@ndcn.ox.ac.uk',
      url='https://git.fmrib.ox.ac.uk/ndcn0236/gyralcoord',
      packages=['gyralcoord', 'gyralcoord.tests'],
      install_requires=dependencies,
      scripts=['scripts/gcoord_gen', 'scripts/gcoord_split', 'scripts/gcoord_transition', 'scripts/gcoord_mult',
               'scripts/gcoord_gui'],
     )
