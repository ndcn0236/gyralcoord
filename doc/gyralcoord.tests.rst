gyralcoord\.tests package
=========================

Submodules
----------

gyralcoord\.tests\.generate\_mesh module
----------------------------------------

.. automodule:: gyralcoord.tests.generate_mesh
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.tests\.test\_cortex module
--------------------------------------

.. automodule:: gyralcoord.tests.test_cortex
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.tests\.test\_cortical\_mesh module
----------------------------------------------

.. automodule:: gyralcoord.tests.test_cortical_mesh
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.tests\.test\_grid module
------------------------------------

.. automodule:: gyralcoord.tests.test_grid
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.tests\.test\_mesh module
------------------------------------

.. automodule:: gyralcoord.tests.test_mesh
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.tests\.test\_orientation module
-------------------------------------------

.. automodule:: gyralcoord.tests.test_orientation
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gyralcoord.tests
    :members:
    :undoc-members:
    :show-inheritance:
