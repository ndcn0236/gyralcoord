.. gyralcoord documentation master file, created by
   sphinx-quickstart on Sat May 18 20:17:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gyralcoord's documentation!
======================================

.. automodule:: gyralcoord

.. toctree::
   :maxdepth: 2

   gyralcoord




Indices and tables
==================

* :doc:`gyralcoord`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
