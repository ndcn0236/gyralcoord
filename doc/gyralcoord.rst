gyralcoord package
==================

Subpackages
-----------

.. toctree::

    gyralcoord.tests

Submodules
----------

gyralcoord\.cortex module
-------------------------

.. automodule:: gyralcoord.cortex
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.cortical\_mesh module
---------------------------------

.. automodule:: gyralcoord.cortical_mesh
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.grid module
-----------------------

.. automodule:: gyralcoord.grid
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.mesh module
-----------------------

.. automodule:: gyralcoord.mesh
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.orientation module
------------------------------

.. automodule:: gyralcoord.orientation
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.radial\_transition module
-------------------------------------

.. automodule:: gyralcoord.radial_transition
    :members:
    :undoc-members:
    :show-inheritance:

gyralcoord\.utils module
------------------------

.. automodule:: gyralcoord.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gyralcoord
    :members:
    :undoc-members:
    :show-inheritance:
